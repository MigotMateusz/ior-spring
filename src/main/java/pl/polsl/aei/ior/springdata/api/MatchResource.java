package pl.polsl.aei.ior.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.polsl.aei.ior.springdata.entity.Match;
import pl.polsl.aei.ior.springdata.repository.MatchRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/matches")
public class MatchResource {

    @Autowired
    MatchRepository matchRepository;

    //http://localhost:8081/matches
    @GetMapping
    List<Match> getAllMatches() {
        return matchRepository.findAll();
    }

    //http://localhost:8081/matches/date/2022-12-31/result/3:2
    @GetMapping("/date/{date}/result/{result}")
    List<Match> getMatchesByStadiumIdAndResult(@PathVariable String date, @PathVariable String result) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return matchRepository.findByDateAndResult(LocalDate.parse(date, dtf), result);
    }

    //http://localhost:8081/matches/distinct/stadiumName/Narodowy/or/result/1:1
    @GetMapping("distinct/stadiumName/{stadiumName}/or/result/{result}")
    List<Match> getDistinctMatchesByStadiumIdAndResult(@PathVariable String stadiumName, @PathVariable String result) {
        return matchRepository.findDistinctByStadium_NameOrResult(stadiumName, result);
    }

    //http://localhost:8081/matches/v2/date/2022-12-31/result/3:2?page=0&size=2&sort=result,ASC
    @GetMapping(value = "v2/date/{date}/result/{result}", params = {"page", "size", "sort"})
    List<Match> getMatchesByStadiumIdAndResultWithPaginationAndSorting(@PathVariable String date,
                                                                       @PathVariable String result,
                                                                       @RequestParam("page") int page,
                                                                       @RequestParam("size") int size,
                                                                       @RequestParam("sort") String sort) {
        SortBuilder sortBuilder = new SortBuilder(sort);
        Pageable pageable = PageRequest.of(page, size, sortBuilder.getSort());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return matchRepository.findByDateAndResult(LocalDate.parse(date, dtf), result, pageable);
    }

    //http://localhost:8081/matches/stadiumNamePrefix/narod
    @GetMapping(value = "stadiumNamePrefix/{namePrefix}")
    List<Match> getMatchesByResult(@PathVariable String namePrefix) {
        namePrefix = namePrefix + "%";
        return matchRepository.findByStadium_NameLikeIgnoreCase(namePrefix);
    }

    //http://localhost:8081/matches/result/1:1/idFloorValue/1
    @GetMapping(value = "result/{result}/idFloorValue/{id}")
    List<Match> findMatchesByResult(@PathVariable String result, @PathVariable Long id) {
        return matchRepository.findMatchesByResultWithBiggerIdThan(result, id);
    }

    //http://localhost:8081/matches/delete/id/1
    @DeleteMapping(value = "delete/id/{id}")
    void deleteMatchById(@PathVariable Long id) {
        matchRepository.deleteById(id);
    }
}
