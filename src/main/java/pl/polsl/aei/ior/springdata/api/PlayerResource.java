package pl.polsl.aei.ior.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.polsl.aei.ior.springdata.entity.Player;
import pl.polsl.aei.ior.springdata.repository.PlayerRepository;

import java.util.List;

@RestController
@RequestMapping("/players")
public class PlayerResource {
    @Autowired
    PlayerRepository playerRepository;

    // http://localhost:8081/players
    @GetMapping
    List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    // http://localhost:8081/players/firstname/Martyna/country/Polska
    @GetMapping("/firstname/{firstname}/country/{country}")
    List<Player> getPlayersByFirstnameAndTeam_Country(@PathVariable String firstname, @PathVariable String country) {
        return playerRepository.findByFirstnameAndTeam_Country(firstname, country);
    }

    // http://localhost:8081/players/distinct/firstname/Martyna/or/country/Polska
    @GetMapping("distinct/firstname/{firstname}/or/country/{country}")
    List<Player> getDistinctPlayersByFirstnameAndTeam_Country(@PathVariable String firstname, @PathVariable String country) {
        return playerRepository.findDistinctByFirstnameOrTeam_Country(firstname, country);
    }

    // http://localhost:8081/players/v2/firstname/Martyna/country/Polska?page=0&size=5&sort=id,DESC
    @GetMapping(value = "v2/firstname/{firstname}/country/{country}", params = {"page", "size", "sort"})
    List<Player> getPlayersByFirstnameAndTeam_CountryWithPaginationAndSorting(@PathVariable String firstname,
                                                                       @PathVariable String country,
                                                                       @RequestParam("page") int page,
                                                                       @RequestParam("size") int size,
                                                                       @RequestParam("sort") String sort) {
        SortBuilder sortBuilder = new SortBuilder(sort);
        Pageable pageable = PageRequest.of(page, size, sortBuilder.getSort());
        return playerRepository.findByFirstnameAndTeam_Country(firstname, country, pageable);
    }

    // http://localhost:8081/players/teamCountryPrefix/Pol
    @GetMapping(value = "teamCountryPrefix/{namePrefix}")
    List<Player> getPlayersByCountry(@PathVariable String namePrefix) {
        namePrefix = namePrefix + "%";
        return playerRepository.findByTeam_CountryLikeIgnoreCase(namePrefix);
    }

    // http://localhost:8081/players/firstname/Martyna/coach/8
    @GetMapping(value = "firstname/{firstname}/coach/{coach}")
    List<Player> findPlayersByTeam_Coach(@PathVariable String firstname, @PathVariable Long coach) {
        return playerRepository.findPlayersByTeam_Coach(firstname, coach);
    }

    // http://localhost:8081/players/delete/id/6
    @DeleteMapping(value = "delete/id/{id}")
    void deleteMatchById(@PathVariable Long id) {playerRepository.deleteById(id);}
}
