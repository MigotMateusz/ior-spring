package pl.polsl.aei.ior.springdata.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.Cascade;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@PrimaryKeyJoinColumn(name = "PLAYER_ID", foreignKey = @ForeignKey(name = "FK_PLAYER_PER"))
@Table(name = "PLAYER")
public class Player extends Person implements Serializable {

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "TEAM_ID", foreignKey = @ForeignKey(name = "FK_PLAYER_TEAM"))
    @JsonIgnore
    private Team team;

    @OneToMany(mappedBy = "player")
    private Set<Position> position = new HashSet<>();

    public Player() {
    }

    public Player(String firstname, String surname) {
        super(firstname, surname);
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return (super.getFirstname() + " "
                + super.getSurname());
    }
}

