package pl.polsl.aei.ior.springdata.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.polsl.aei.ior.springdata.entity.Stadium;

import java.time.LocalDate;
import java.util.List;

public interface StadiumRepository extends JpaRepository<Stadium, Integer> {
    List<Stadium> findByMatches_DateAndLocation(LocalDate date, String location);

    List<Stadium> findByMatches_DateAndLocation(LocalDate date, String result, Pageable pageable);

    List<Stadium> findByMatches_ResultLikeIgnoreCase(String firstTeamScore);

    List<Stadium> findDistinctByMatches_DateOrName(LocalDate date, String name);

    @Query("select s from Stadium s where s.id > :idFloorValue and location = :location")
    List<Stadium> findStadiumsByLocationWithBiggerIdThan(@Param("location") String location, @Param("idFloorValue") Long value);

    @Transactional
    void deleteById(Long id);
}
