package pl.polsl.aei.ior.springdata.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import pl.polsl.aei.ior.springdata.entity.Stadium;
import pl.polsl.aei.ior.springdata.repository.StadiumRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("/stadiums")
public class StadiumResource {

    @Autowired
    StadiumRepository stadiumRepository;

    //http://localhost:8081/stadiums/date/2022-12-30/location/Wawa
    @GetMapping("/date/{date}/location/{location}")
    List<Stadium> getStadiumsByMatchesDateAndLocation(@PathVariable String date, @PathVariable String location) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return stadiumRepository.findByMatches_DateAndLocation(LocalDate.parse(date, dtf), location);
    }

    //http://localhost:8081/v2/stadiums/date/2022-12-30/location/Wawa?page=0&size=2&sort=result,ASC
    @GetMapping(value = "v2/date/{date}/location/{location}", params = {"page", "size", "sort"})
    List<Stadium> getStadiumsByMatchesDateAndLocationWithPaginationAndSorting(@PathVariable String date,
                                                                              @PathVariable String location,
                                                                              @RequestParam("page") int page,
                                                                              @RequestParam("size") int size,
                                                                              @RequestParam("sort") String sort) {
        SortBuilder sortBuilder = new SortBuilder(sort);
        Pageable pageable = PageRequest.of(page, size, sortBuilder.getSort());
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return stadiumRepository.findByMatches_DateAndLocation(LocalDate.parse(date, dtf), location, pageable);
    }

    //http://localhost:8081/stadiums/firstTeamScore/1
    @GetMapping(value = "firstTeamScore/{firstTeamScore}")
    List<Stadium> getStadiumByFirstTeamScore(@PathVariable String firstTeamScore) {
        firstTeamScore = firstTeamScore + ":%";
        return stadiumRepository.findByMatches_ResultLikeIgnoreCase(firstTeamScore);
    }

    //http://localhost:8081/stadiums/distinct/date/2022-12-20/or/name/Narodowy
    @GetMapping("distinct/date/{date}/or/name/{name}")
    List<Stadium> findDistinctStadiumsByMatchesDateOrName(@PathVariable String date, @PathVariable String name) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return stadiumRepository.findDistinctByMatches_DateOrName(LocalDate.parse(date, dtf), name);
    }

    //http://localhost:8081/stadiums/location/Wawa/idFloorValue/1
    @GetMapping(value = "location/{location}/idFloorValue/{id}")
    List<Stadium> findMatchesByResult(@PathVariable String location, @PathVariable Long id) {
        return stadiumRepository.findStadiumsByLocationWithBiggerIdThan(location, id);
    }

    //http://localhost:8081/stadiums/delete/id/1
    @DeleteMapping(value = "delete/id/{id}")
    void deleteMatchById(@PathVariable Long id) {
        stadiumRepository.deleteById(id);
    }
}
