package pl.polsl.aei.ior.springdata.entity;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "STADIUM")
public class Stadium implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STADIUM_ID", nullable = false)
    private int id;

    private String name;
    private String location;

    @OneToMany(mappedBy = "stadium", orphanRemoval = true)
    private Set<Match> matches = new HashSet<>();

    public Stadium() {
    }

    public Stadium(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Set<Match> getMatches() {
        return matches;
    }

    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return ("name: " + name + " "
                + "location: " + location + "  "
                + " match: " + matches);
    }
}
