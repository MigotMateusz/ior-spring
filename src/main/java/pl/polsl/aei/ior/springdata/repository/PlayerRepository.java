package pl.polsl.aei.ior.springdata.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.polsl.aei.ior.springdata.entity.Player;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Integer> {
    List<Player> findAll();

    List<Player> findByFirstnameAndTeam_Country(String firstname, String country);

    List<Player> findDistinctByFirstnameOrTeam_Country(String firstname, String country);

    List<Player> findByFirstnameAndTeam_Country(String firstname, String country, Pageable pageable);

    List<Player> findByTeam_CountryLikeIgnoreCase(String namePrefix);

    @Query("select p from Player p, Team t where t.id=p.team and t.coach.id = :coach and firstname = :firstname")
    List<Player> findPlayersByTeam_Coach(@Param("firstname") String firstname, @Param("coach") Long coach);

    @Transactional
    void deleteById(Long id);
}
