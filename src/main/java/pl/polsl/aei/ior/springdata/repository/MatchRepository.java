package pl.polsl.aei.ior.springdata.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pl.polsl.aei.ior.springdata.entity.Match;

import java.time.LocalDate;
import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Integer> {

    List<Match> findAll();

    List<Match> findByDateAndResult(LocalDate date, String result);

    List<Match> findDistinctByStadium_NameOrResult(String stadiumName, String result);

    List<Match> findByDateAndResult(LocalDate date, String result, Pageable pageable);

    List<Match> findByStadium_NameLikeIgnoreCase(String namePrefix);

    @Query("select m from Match m where m.id > :idFloorValue and result = :result")
    List<Match> findMatchesByResultWithBiggerIdThan(@Param("result") String result, @Param("idFloorValue") Long value);

    @Transactional
    void deleteById(Long id);
}
