package pl.polsl.aei.ior.springdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IorSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(IorSpringApplication.class, args);
    }

}
